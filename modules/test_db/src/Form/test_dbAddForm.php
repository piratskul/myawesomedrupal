<?php

namespace Drupal\test_db\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\test_db\test_dbStorage;
use Drupal\file\Entity\File;
/**
 * Simple form to add an entry, with all the interesting fields.
 */
class test_dbAddForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'test_db_add_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = array();

    $form['message'] = array(
      '#markup' => $this->t('Add an entry to the Gallery.'),
    );
   
    $form['add'] = array(
      '#type' => 'fieldset',
      '#title' => t('Add a item entry'),
    );
    $form['add']['tag'] = array(
      '#type' => 'textfield',
      '#title' => t('Tag'),
      '#size' => 15,
    );    
    $form['add']['title'] = array(
      '#type' => 'textfield',
      '#title' => t('Title'),
      '#size' => 15,
    );
    $form['add']['body'] = array(
      '#type' => 'textfield',
      '#title' => t('Body'),
      '#size' => 15,
    );
    $form['add']['img'] = array(
      '#type' => 'managed_file',
      '#title' => t('Items Picture'),
      '#multiple' => FALSE,      
      '#upload_validators' => array(
          'file_validate_is_image'      => array(),
          'file_validate_extensions' => array('gif png jpg jpeg'),
          'file_validate_size' => array(25600000),
      ),
      '#theme' => 'image_widget',
      '#preview_image_style' => 'medium',
      '#upload_location' => 'public://test-pictures',
      '#required' => TRUE,
    );
    $form['add']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Add'),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Confirm that age is numeric.
    
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Gather the current user so the new record has ownership.
    $account = $this->currentUser();
    /* Fetch the array of the file stored temporarily in database */ 
    $image = $form_state->getValue('img');
    /* Load the object of the file by it's fid */ 
    $file = \Drupal\file\Entity\File::load( $image[0] ); 
    $file_name = $file->getFilename();
    $file->status = FILE_STATUS_PERMANENT;
    $file->save();
    // Save the submitted entry.    
    $entry = array(
      'tag' => $form_state->getValue('tag'),
      'title' => $form_state->getValue('title'),
      'body' => $form_state->getValue('body'),
      'img' => '/Knife/sites/default/files/test-pictures/' . $file_name,
      'uid' => $account->id(),
    );
    $return = test_dbStorage::insert($entry);
    if ($return) {
      drupal_set_message(t('Created entry @entry', array('@entry' => print_r($entry, TRUE))));
    }
  }

}
