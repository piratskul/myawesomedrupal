<?php

namespace Drupal\test_db\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\test_db\test_dbStorage;

/**
 * Sample UI to update a record.
 */
class test_dbUpdateForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'test_db_update_form';
  }

  /**
   * Sample UI to update a record.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Wrap the form in a div.
    $form = array(
      '#prefix' => '<div id="updateform">',
      '#suffix' => '</div>',
    );
    // Add some explanatory text to the form.
    $form['message'] = array(
      '#markup' => $this->t('Demonstrates a database update operation.'),
    );
    // Query for items to display.
    $entries = test_dbStorage::load();
    // Tell the user if there is nothing to display.
    if (empty($entries)) {
      $form['no_values'] = array(
        '#value' => t('No entries exist in the table test_db table.'),
      );
      return $form;
    }

    $keyed_entries = array();
    foreach ($entries as $entry) {
      $options[$entry->id] = t('@id: @title', array(
        '@id' => $entry->id,        
        '@title' => $entry->title,        
      ));
      $keyed_entries[$entry->id] = $entry;
    }

    // Grab the id.
    $id = $form_state->getValue('id');
    // Use the pid to set the default entry for updating.
    $default_entry = !empty($id) ? $keyed_entries[$id] : $entries[0];

    // Save the entries into the $form_state. We do this so the AJAX callback
    // doesn't need to repeat the query.
    $form_state->setValue('entries', $keyed_entries);

    $form['id'] = array(
      '#type' => 'select',
      '#options' => $options,
      '#title' => t('Choose item to update'),
      '#default_value' => $default_entry->id,
      '#ajax' => array(
        'wrapper' => 'updateform',
        'callback' => array($this, 'updateCallback'),
      ),
    );

    $form['tag'] = array(
      '#type' => 'textfield',
      '#title' => t('Updated tag'),
      '#size' => 15,
      '#default_value' => $default_entry->tag,
    );

    $form['title'] = array(
      '#type' => 'textfield',
      '#title' => t('Updated title'),
      '#size' => 15,
      '#default_value' => $default_entry->title,
    );

    $form['body'] = array(
      '#type' => 'textfield',
      '#title' => t('Updated body'),
      '#size' => 15,
      '#default_value' => $default_entry->body,
    );

    $form['img'] = array(
      '#type' => 'hidden',
      '#title' => t('Current img'),
      '#default_value' => $default_entry->img,      
    );

    $form['new_img'] = array(
      '#type' => 'managed_file',
      '#title' => t('Updated img'),
      '#default_value' => $default_entry->img,
      '#upload_validators' => array(
          'file_validate_extensions' => array('gif png jpg jpeg'),
          'file_validate_size' => array(25600000),
      ),
      '#theme' => 'image_widget',
      '#preview_image_style' => 'medium',
      '#upload_location' => 'public://profile-pictures',      
    );

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Update'),
    );
    return $form;
  }

  /**
   * AJAX callback handler for the id select.
   *
   * When the pid changes, populates the defaults from the database in the form.
   */
  public function updateCallback(array $form, FormStateInterface $form_state) {
    // Gather the DB results from $form_state.
    $entries = $form_state->getValue('entries');
    // Use the specific entry for this $form_state.
    $entry = $entries[$form_state->getValue('id')];
    // Setting the #value of items is the only way I was able to figure out
    // to get replaced defaults on these items. #default_value will not do it
    // and shouldn't.
    foreach (array('title', 'body', 'img') as $item) {
      $form[$item]['#value'] = $entry->$item;
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Confirm that age is numeric.
    
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Gather the current user so the new record has ownership.
    $account = $this->currentUser();
    /* Fetch the array of the file stored temporarily in database */ 
    $image = $form_state->getValue('new_img');
    $img = $form_state->getValue('img'); 
    /* Load the object of the file by it's fid */ 
    $file = \Drupal\file\Entity\File::load( $image[0] );   
    if (isset($file)) {      
      $file_name = $file->getFilename();
      $file_url = '/sites/default/files/test-pictures/' . $file_name;
      $img = $file_url;
    };

    // Save the submitted entry.
    $entry = array(
      'id' => $form_state->getValue('id'),
      'title' => $form_state->getValue('title'),
      'body' => $form_state->getValue('body'),
      'img' =>  $img,
      'uid' => $account->id(),
    );
    $count = test_dbStorage::update($entry);
    drupal_set_message(t('Updated entry @entry (@count row updated)', array(
      '@count' => $count,
      '@entry' => print_r($entry, TRUE),
    )));
  }

}
