<?php

namespace Drupal\test_db\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\test_db\test_dbStorage;
use Drupal\image\Entity\ImageStyle;

/**
 * Controller for DBTNG Example.
 */
class test_dbController extends ControllerBase {

  /**
   * Render a list of entries in the database.
   */
  public function tagList($tag) {
    $content = array();
    $rods = array();

    foreach ($entries = test_dbStorage::tag_load($tag) as $entry) {      
       $rods[] = array('data' => (array) $entry);      
    }      
    
    $content['block'][0] = array(
      '#theme' => 'test_db_template',
      '#rods' => $rods,
      '#empty' => t('No entries available.'),
    ); 

    $content['#cache']['max-age'] = 0;
        
    return $content;
  }

  public function entryList() {
    $content = array();

    $i = 0;
    $blocks = 1;  
    $new_blocks = 0;

    $rows = array();    

    foreach ($entries = test_dbStorage::load() as $entry) {      
      $rows[] = array('data' => (array) $entry);  
    } 

    function new_rows($rows, $num) {
        $such_rows = array_slice($rows, $num*20, 20, true);
      return $such_rows;
    }

    for ($i = 0; $i < $blocks; $i++) {
      $content['block'][$i] = [
      '#theme' => 'test_db_template',
      '#rows' => new_rows($rows, $i),      
      '#empty' => t('No entries available.'),
      ];
    }      
    
    // Don't cache this page.
    $content['#cache']['max-age'] = 0;
        
    return $content;    
  }  

   

}
