<?php

namespace Drupal\general\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\general\generalStorage;

/**
 * Class DefaultController.
 *
 * @package Drupal\general\Controller
 */
class DefaultController extends ControllerBase {

  /**
   * Index.
   *
   * @return string
   *   Return Hello string.
   */
  public function index() {
    $content = array();
    $lvls = array();    

    foreach ($entries = generalStorage::load() as $entry) {      
      $lvls[] = array('data' => (array) $entry);  
    }    

    $content['block'] = array(
      '#theme' => 'general',
      '#lvls' => $lvls,
      '#empty' => t('No entries available.'),
    ); 

    return $content;
  }

  // AJAX Callback to read a message.
  public function newQuestCallback($method, $mid) {
    $message = general_load_message($mid);
    // Create AJAX Response object.
    $response = new AjaxResponse();
    // Call the readMessage javascript function.
    $response->addCommand( new NeqQuestCommand($message));   
   // Return ajax response.
   return $response;
  }

}
