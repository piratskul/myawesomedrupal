(function ($) {
Drupal.behaviors.generalbehavior = {
  attach: function (context, settings) {  	
	var cards = $(".quest-btn");
 	for (var i = 0; i < cards.length; i++) {
 		var target1 = Math.floor(Math.random() * cards.length - 1) + 1;
  		var target2 = Math.floor(Math.random() * cards.length - 1) + 1;
 		cards.eq(target1).before(cards.eq(target2));
	}
	console.log('Answers randomed!');
	
	var n = 0;
	var cur_score = 0;
	var points = 0;
	var res_score =0;

	$("#quest-ans-1").on("click", function(){	
		n++;

		$(this).blur();

		if (lvl[n] == null) {			
			res_score = res_score+100;

			alert("You win with " + res_score + " points. Good job!");
			location.reload();
		}
		else {
			cur_score = +($("#quest-score").text());	
			points = lvl[n]*25;
			res_score = cur_score + points;

			$("#quest-score").text(res_score);	

			$(".quest-title").text(que[n]);
			$("#quest-lvl").text("Lvl: " + lvl[n]);	
			$("#quest-ans-1").text(ans[n]);
			$("#quest-ans-2").text(wans1[n]);
			$("#quest-ans-3").text(wans2[n]);
			$("#quest-ans-4").text(wans3[n]);			

			for (var i = 0; i < cards.length; i++) {
	 			var target1 = Math.floor(Math.random() * cards.length - 1) + 1;
	  			var target2 = Math.floor(Math.random() * cards.length - 1) + 1;
	 			cards.eq(target1).before(cards.eq(target2));
			}
		}
	});

	$(".w_ans").on("click", function(){	
		n++;

		$(this).blur();
		if (lvl[n] == null) {			
			res_score = res_score-100;

			alert("You win with " + res_score + " points. Good job!");
			location.reload();
		}
		else {
			cur_score = +($("#quest-score").text());	
			points = lvl[n]*25;
			res_score = cur_score - points;

			$("#quest-score").text(res_score);	

			$(".quest-title").text(que[n]);
			$("#quest-lvl").text("Lvl: " + lvl[n]);	
			$("#quest-ans-1").text(ans[n]);
			$("#quest-ans-2").text(wans1[n]);
			$("#quest-ans-3").text(wans2[n]);
			$("#quest-ans-4").text(wans3[n]);			

			for (var i = 0; i < cards.length; i++) {
	 			var target1 = Math.floor(Math.random() * cards.length - 1) + 1;
	  			var target2 = Math.floor(Math.random() * cards.length - 1) + 1;
	 			cards.eq(target1).before(cards.eq(target2));
			}
		}
	});
	

  }
};




})(jQuery);
