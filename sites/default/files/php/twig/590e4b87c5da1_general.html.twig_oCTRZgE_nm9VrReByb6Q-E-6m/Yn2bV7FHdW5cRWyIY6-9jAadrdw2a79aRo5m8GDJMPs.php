<?php

/* modules/general/templates/general.html.twig */
class __TwigTemplate_0bfac005cfc4e86b2ca7743ee27f2c6ca39c9954348f9bd649a402e462c8735b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("set" => 3, "if" => 10, "for" => 48);
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('sandbox')->checkSecurity(
                array('set', 'if', 'for'),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setTemplateFile($this->getTemplateName());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 1
        echo "<!-- Add you custom twig html here -->
";
        // line 3
        $context["item_classes"] = array(0 => "quest");
        // line 7
        echo "

<div align=\"center\" class=\"template\">";
        // line 10
        if ((isset($context["lvls"]) ? $context["lvls"] : null)) {
            // line 11
            echo "<div id=\"quest-";
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["i"]) ? $context["i"] : null), "html", null, true));
            echo "\" ";
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["attributes"]) ? $context["attributes"] : null), "addClass", array(0 => (isset($context["item_classes"]) ? $context["item_classes"] : null)), "method"), "html", null, true));
            echo " >
\t\t<div class=\"panel panel-success quest-panel\">
\t\t\t<div class=\"panel-heading quest-heading\">
\t\t   \t\t<h1 class=\"quest-title\">";
            // line 14
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["lvls"]) ? $context["lvls"] : null), 0, array()), "data", array()), "question", array()), "html", null, true));
            echo "</h1>
\t\t\t</div>

\t\t\t<div class=\"panel-body quest-body\">
\t\t\t\t<div class=\"quest-lvl\">
\t\t\t\t\t<label id=\"q-lvl-label\">Lvl:</label>
\t\t  \t\t\t<p id=\"quest-lvl\">";
            // line 20
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["lvls"]) ? $context["lvls"] : null), 0, array()), "data", array()), "lvl", array()), "html", null, true));
            echo "</p>
\t\t\t\t</div>\t\t\t\t

\t\t\t\t<div class=\"quest-score\">
\t\t\t\t\t<label id=\"q-sc-label\">Score:</label>
\t\t  \t\t\t<p id=\"quest-score\">0</p>
\t\t\t\t</div>
\t\t\t\t
\t\t  \t\t<button id=\"quest-ans-1\" class=\"quest-btn btn btn-default\">";
            // line 28
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["lvls"]) ? $context["lvls"] : null), 0, array()), "data", array()), "answer", array()), "html", null, true));
            echo "</button>\t  \t\t\t
\t\t\t\t<button id=\"quest-ans-2\" class=\"w_ans quest-btn btn btn-default\">";
            // line 29
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["lvls"]) ? $context["lvls"] : null), 0, array()), "data", array()), "w_answer_1", array()), "html", null, true));
            echo "</button>
\t\t\t\t<button id=\"quest-ans-3\" class=\"w_ans quest-btn btn btn-default\">";
            // line 30
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["lvls"]) ? $context["lvls"] : null), 0, array()), "data", array()), "w_answer_2", array()), "html", null, true));
            echo "</button>
\t\t\t\t<button id=\"quest-ans-4\" class=\"w_ans quest-btn btn btn-default\">";
            // line 31
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["lvls"]) ? $context["lvls"] : null), 0, array()), "data", array()), "w_answer_3", array()), "html", null, true));
            echo "</button>
\t\t  \t</div>
\t\t</div>\t
\t</div>";
        }
        // line 36
        echo "</div>


<script>
\tvar que = new Array();
\tvar lvl = new Array();
\tvar ans = new Array();
\tvar wans1 = new Array();
\tvar wans2 = new Array();
\tvar wans3 = new Array();
</script>

";
        // line 48
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["lvls"]) ? $context["lvls"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["lvl"]) {
            // line 49
            echo "\t<script>
\t\tque.push('";
            // line 50
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($this->getAttribute($context["lvl"], "data", array()), "question", array()), "html", null, true));
            echo "');\t
\t\tlvl.push('";
            // line 51
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($this->getAttribute($context["lvl"], "data", array()), "lvl", array()), "html", null, true));
            echo "');
\t\tans.push('";
            // line 52
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($this->getAttribute($context["lvl"], "data", array()), "answer", array()), "html", null, true));
            echo "');\t\t
\t\twans1.push('";
            // line 53
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($this->getAttribute($context["lvl"], "data", array()), "w_answer_1", array()), "html", null, true));
            echo "');\t
\t\twans2.push('";
            // line 54
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($this->getAttribute($context["lvl"], "data", array()), "w_answer_2", array()), "html", null, true));
            echo "');\t
\t\twans3.push('";
            // line 55
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($this->getAttribute($context["lvl"], "data", array()), "w_answer_3", array()), "html", null, true));
            echo "');\t\t
\t</script>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['lvl'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    public function getTemplateName()
    {
        return "modules/general/templates/general.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  143 => 55,  139 => 54,  135 => 53,  131 => 52,  127 => 51,  123 => 50,  120 => 49,  116 => 48,  102 => 36,  95 => 31,  91 => 30,  87 => 29,  83 => 28,  72 => 20,  63 => 14,  54 => 11,  52 => 10,  48 => 7,  46 => 3,  43 => 1,);
    }

    public function getSource()
    {
        return "<!-- Add you custom twig html here -->
{%
  set item_classes = [
    'quest',    
  ]
%}


<div align=\"center\" class=\"template\">\t
{%- if lvls -%}\t\t\t\t   
\t<div id=\"quest-{{ i }}\" {{ attributes.addClass(item_classes) }} >
\t\t<div class=\"panel panel-success quest-panel\">
\t\t\t<div class=\"panel-heading quest-heading\">
\t\t   \t\t<h1 class=\"quest-title\">{{ lvls.0.data.question }}</h1>
\t\t\t</div>

\t\t\t<div class=\"panel-body quest-body\">
\t\t\t\t<div class=\"quest-lvl\">
\t\t\t\t\t<label id=\"q-lvl-label\">Lvl:</label>
\t\t  \t\t\t<p id=\"quest-lvl\">{{ lvls.0.data.lvl }}</p>
\t\t\t\t</div>\t\t\t\t

\t\t\t\t<div class=\"quest-score\">
\t\t\t\t\t<label id=\"q-sc-label\">Score:</label>
\t\t  \t\t\t<p id=\"quest-score\">0</p>
\t\t\t\t</div>
\t\t\t\t
\t\t  \t\t<button id=\"quest-ans-1\" class=\"quest-btn btn btn-default\">{{ lvls.0.data.answer }}</button>\t  \t\t\t
\t\t\t\t<button id=\"quest-ans-2\" class=\"w_ans quest-btn btn btn-default\">{{ lvls.0.data.w_answer_1 }}</button>
\t\t\t\t<button id=\"quest-ans-3\" class=\"w_ans quest-btn btn btn-default\">{{ lvls.0.data.w_answer_2 }}</button>
\t\t\t\t<button id=\"quest-ans-4\" class=\"w_ans quest-btn btn btn-default\">{{ lvls.0.data.w_answer_3 }}</button>
\t\t  \t</div>
\t\t</div>\t
\t</div>
{%- endif -%}
</div>


<script>
\tvar que = new Array();
\tvar lvl = new Array();
\tvar ans = new Array();
\tvar wans1 = new Array();
\tvar wans2 = new Array();
\tvar wans3 = new Array();
</script>

{% for lvl in lvls %}
\t<script>
\t\tque.push('{{ lvl.data.question }}');\t
\t\tlvl.push('{{ lvl.data.lvl }}');
\t\tans.push('{{ lvl.data.answer }}');\t\t
\t\twans1.push('{{ lvl.data.w_answer_1 }}');\t
\t\twans2.push('{{ lvl.data.w_answer_2 }}');\t
\t\twans3.push('{{ lvl.data.w_answer_3 }}');\t\t
\t</script>
{% endfor %}
";
    }
}
