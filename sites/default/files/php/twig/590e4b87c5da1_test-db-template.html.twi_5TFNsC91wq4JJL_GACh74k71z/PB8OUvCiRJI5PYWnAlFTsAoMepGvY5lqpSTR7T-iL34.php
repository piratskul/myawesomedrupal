<?php

/* modules/test_db/templates/test-db-template.html.twig */
class __TwigTemplate_086f970b9b21ec3f6816b5505b908d6a0996023c8aa2130068b2015462db3707 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("set" => 9, "if" => 17, "for" => 18);
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('sandbox')->checkSecurity(
                array('set', 'if', 'for'),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setTemplateFile($this->getTemplateName());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 9
        $context["item_classes"] = array(0 => "item");
        // line 13
        echo "
";
        // line 14
        $context["break"] = false;
        // line 15
        echo "
<div align=\"center\" class=\"template\">";
        // line 17
        if ((isset($context["rows"]) ? $context["rows"] : null)) {
            // line 18
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["rows"]) ? $context["rows"] : null));
            foreach ($context['_seq'] as $context["i"] => $context["row"]) {
                if ( !(isset($context["break"]) ? $context["break"] : null)) {
                    // line 19
                    echo "\t\t<div id=\"item-";
                    echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $context["i"], "html", null, true));
                    echo "\" ";
                    echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["attributes"]) ? $context["attributes"] : null), "addClass", array(0 => (isset($context["item_classes"]) ? $context["item_classes"] : null)), "method"), "html", null, true));
                    echo " >
\t\t\t<div class=\"item-title \">
\t\t\t\t<a class=\"item-link\" href=\"/Knife/test_db/show/";
                    // line 21
                    echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($this->getAttribute($context["row"], "data", array()), "tag", array()), "html", null, true));
                    echo "\">";
                    echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($this->getAttribute($context["row"], "data", array()), "tag", array()), "html", null, true));
                    echo "</a>
\t\t\t</div>\t\t \t
\t\t\t<div class=\"item-body\">";
                    // line 23
                    echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($this->getAttribute($context["row"], "data", array()), "title", array()), "html", null, true));
                    echo "</div>
\t\t\t<img src=\"";
                    // line 24
                    echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($this->getAttribute($context["row"], "data", array()), "img", array()), "html", null, true));
                    echo "\">
\t\t</div>

\t\t";
                    // line 27
                    if (($context["i"] == 5)) {
                        // line 28
                        echo "\t       \t<hr>
\t   \t";
                    }
                    // line 29
                    echo "\t

\t";
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['i'], $context['row'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
        // line 34
        if ((isset($context["rods"]) ? $context["rods"] : null)) {
            // line 35
            echo "<h1>Rods exist</h1>
\t";
            // line 36
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["rods"]) ? $context["rods"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["rod"]) {
                // line 37
                echo "\t\t<div class=\"tag-item\" }} >
\t\t\t<div class=\"item-title \">
\t\t\t\t<a class=\"item-link\" href=\"#\">";
                // line 39
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($this->getAttribute($context["rod"], "data", array()), "tag", array()), "html", null, true));
                echo "</a>
\t\t\t</div>\t
\t\t\t<div class=\"item-body\">";
                // line 41
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($this->getAttribute($context["rod"], "data", array()), "title", array()), "html", null, true));
                echo "</div>
\t\t\t<img src=\"";
                // line 42
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($this->getAttribute($context["rod"], "data", array()), "img", array()), "html", null, true));
                echo "\">\t\t
\t\t</div>\t\t\t\t\t   \t
\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['rod'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
        // line 47
        echo "</div>


";
    }

    public function getTemplateName()
    {
        return "modules/test_db/templates/test-db-template.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  133 => 47,  123 => 42,  119 => 41,  114 => 39,  110 => 37,  106 => 36,  103 => 35,  101 => 34,  91 => 29,  87 => 28,  85 => 27,  79 => 24,  75 => 23,  68 => 21,  60 => 19,  55 => 18,  53 => 17,  50 => 15,  48 => 14,  45 => 13,  43 => 9,);
    }

    public function getSource()
    {
        return "{#
/**
* @file
* Default theme implementation for my template.
*
*/
#}
{%
  set item_classes = [
    'item',    
  ]
%}

{% set break = false %}

<div align=\"center\" class=\"template\">\t
{%- if rows -%}\t\t
\t{% for i, row in rows if not break %}
\t\t<div id=\"item-{{ i }}\" {{ attributes.addClass(item_classes) }} >
\t\t\t<div class=\"item-title \">
\t\t\t\t<a class=\"item-link\" href=\"/Knife/test_db/show/{{ row.data.tag }}\">{{ row.data.tag }}</a>
\t\t\t</div>\t\t \t
\t\t\t<div class=\"item-body\">{{ row.data.title }}</div>
\t\t\t<img src=\"{{ row.data.img }}\">
\t\t</div>

\t\t{% if i == 5 %}
\t       \t<hr>
\t   \t{% endif %}\t

\t{% endfor %}\t\t   
{%- endif -%}\t
\t
{%- if rods -%}\t\t
\t<h1>Rods exist</h1>
\t{% for rod in rods  %}
\t\t<div class=\"tag-item\" }} >
\t\t\t<div class=\"item-title \">
\t\t\t\t<a class=\"item-link\" href=\"#\">{{ rod.data.tag }}</a>
\t\t\t</div>\t
\t\t\t<div class=\"item-body\">{{ rod.data.title }}</div>
\t\t\t<img src=\"{{ rod.data.img }}\">\t\t
\t\t</div>\t\t\t\t\t   \t
\t{% endfor %}
{%- endif -%}

</div>


";
    }
}
